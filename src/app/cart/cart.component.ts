import { Component, OnInit } from '@angular/core';

import { IProduct } from '../products/product';
import { CartService } from './cart.service';
import { CheckoutService } from '../checkout/checkout.service';

@Component({
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  pageTitle = 'Cart';
  imageWidth = 50;
  imageMargin = 2;
  showImage = true;
  errorMessage = '';
  total_price = 0;


  products: IProduct[] = [];

  constructor(
    private cartService: CartService,
    private checkoutService: CheckoutService
  ) { }

  checkoutFinished(finished: boolean) {
    if (finished) {
      this.cartService.clearProducts();
    }
    this.products = this.cartService.getProducts();
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  ngOnInit(): void {
    this.products = this.cartService.getProducts();
    this.total_price = this.cartService.getPrice();
  }
}
